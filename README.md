# Nba Frontend
You see today's match results and ongoing matches' statistics. Page content is refreshed in every 5 seconds.

## Technologies and Tools Used
- Javascript as programming language
- Vue.js
- Nuxt
- Docker
- Docker-compose

## CI/CD
- This repository has only one build-push stage, it builds the frontend and pushes it to the docker registry.
- You can see the pipeline from here: https://gitlab.com/gultekinmsg/nba/-/pipelines
- See the pipeline file from `/.gitlab-ci.yml`

## Quick Start
1. Enter to project's root directory and run backend and frontend with docker-compose :
> docker-compose up -d

**Your frontend url will be localhost:8081*

**Your backend url will be localhost:8080*

## Development
To run frontend for at local:
 - Install dependencies with `yarn install` or `npm install`
 - Run `yarn dev` or `npm run dev`
 - Frontend will be available at localhost:8081

## Self Assessment
- It took about 1 and half hour of development.

## Links
- Backend repository: https://gitlab.com/gultekinmsg/insider-api

## Contributors
Muhammed Said Gültekin (gultekinmsg@gmail.com)
