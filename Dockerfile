FROM node:lts
ENV HOST 0.0.0.0
ENV NUXT_HOST 0.0.0.0
ENV NUXT_PORT 8081
WORKDIR /app
COPY . .
RUN npm install
RUN npm run build
EXPOSE 8081
CMD npm run start
