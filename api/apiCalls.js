import axios from 'axios'

export const getAll = () => {
  return axios.get('/nba/' + getDate(), {
    headers: {
      Accept: 'application/json'
    },
    baseURL: 'http://localhost:8080'
  })
}

function getDate () {
  const today = new Date()
  const year = today.getFullYear()
  const month = today.getMonth() + 1
  const day = today.getDate()
  if (month < 10) {
    return year + '-0' + month + '-' + day
  } else {
    return year + '-' + month + '-' + day
  }
}
